#include <iostream>
class Animal {
public:
	virtual void Voice(){
		std::cout << "Aminal!";
	}
};

class Dog : public Animal {
public:
	void Voice() override {
		std::cout << "Woof!";
	}
};

class Cat : public Animal {
public:
	void Voice() override {
		std::cout << "Meow!";
	}
};

class Duck : public Animal {
public:
	void Voice() override {
		std::cout << "Kwak!";
	}
};

int main() {
	Animal* arr[] { new Dog, new Cat, new Duck };

	auto arrLength = sizeof(arr) / sizeof(*arr);
	
	for (auto a = arr; a < arr + arrLength; a++) {
		(*a)->Voice();
		std::cout << std::endl;
	}
}